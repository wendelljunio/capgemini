<?php 
namespace App\Contracts; 
 
interface IEmployeeRepository { 
    public function create($employee); 
    public function getEmployee(); 
} 