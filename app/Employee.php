<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public function workstations()
    {
        return $this->belongsToMany('App\Workstation')
                        ->using('App\EmployeeWorkstations')
                        ->withPivot([
                            'created_by',
                            'updated_by'
                        ]);
    }
}
