<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\IEmployeeRepository;
use App\Http\Requests\CreateEmployee;

class EmployeeController extends Controller
{
    protected $employeeRepository;
    public function __construct(IEmployeeRepository $iEmployeeRepository){
        $this->employeeRepository = $iEmployeeRepository;
    } 
    public function create(CreateEmployee $employee) {
        $validated = $employee->validated();
        return $this->employeeRepository->create($validated);
    }

    public function getEmployee() {
        return $this->employeeRepository->getEmployee();
    }
}
