<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Contracts\IEmployeeRepository;
use App\Repositories\EmployeeRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IEmployeeRepository::class, EmployeeRepository::class); 
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
