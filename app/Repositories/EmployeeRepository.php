<?php

namespace App\Repositories;

use App\Contracts\IEmployeeRepository;
use App\Employee;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class EmployeeRepository implements IEmployeeRepository{

    public function create($newEmployee) {
        $employee = new Employee;
        $employee->name = $newEmployee['name'];
        $employee->date_of_birth = $newEmployee['dateOfBirth'];
        $employee->city = $newEmployee['city'];
        $employee->telephone = $newEmployee['telephone'];
        $employee->save();
        return ['status' => true, "user" => $employee]; 
    }

    public function getEmployee() {
        
        return DB::table('employees')
            ->join('employee_workstation', 'employee_workstation.employee_id', '=', 'employees.id')
            ->join('workstations', 'workstations.id', '=', 'employee_workstation.workstation_id')
            ->where('workstations.name', '=', 'São Paulo')
            ->where('employee_workstation.enabling_data', '<', Carbon::now()->subDays(30)->format('Y-m-d'))
            ->select('employees.*')
            ->get();
    }
}