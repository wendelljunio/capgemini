<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workstation extends Model
{
    public function employees()
    {
        return $this->belongsToMany('App\Employee')
                        ->using('App\EmployeeWorkstations')
                        ->withPivot([
                            'created_by',
                            'updated_by'
                        ]);
    }
}
