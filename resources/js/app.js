
require('./bootstrap');

import Vue from 'vue'
import router from './router/index.js'
import HomePage from './pages/HomePage.vue'

new Vue({
    el: '#app',
    components: { HomePage },
    router
});
