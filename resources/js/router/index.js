import Vue from 'vue'
import Router from 'vue-router'
import EmployeePage from '../pages/EmployeePage'

Vue.use(Router)

const router = new Router({
    routes: [
        {
            path: '/',
            name: 'EmployeePage',
            component: EmployeePage
        }
    ]
})

export default router

